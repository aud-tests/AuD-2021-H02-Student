# Tests für die zweite Hausübung der AuD im SoSe 2021

Voraussetzung für das Funktionieren der Tests ist, dass die Klasse `ListOfArraysItem` einen Konstruktor ohne Parameter hat. Außerdem sollte eine eigene JUnit Run Configuration angelegt werden, da durch den gradle task nicht alle Meldungen angezeigt werden (insbesondere warum Tests ignoriert werden).

In den Tests befinden sich Kommentare, die über den Aufbau der Listen Aufschluss geben. Das Format ist folgendes: `(<Anzahl der Listenelemente im Array>, {<"sichtbare" Listenelemente> | <"unsichtbare" Listenelemente>})[ <-> (<weiteres list item>)...]`. "sichtbare" Elemente sind Arraykomponenten an einem Index `i < numberOfListElemsInArray`, "unsichtbare" dem entsprechend Komponenten an `i >= numberOfListElemsInArray`.

Beispiele: \
Eine Liste mit einem list item, einer Arraylänge von 4 und den Elementen -3 und 1: `(2, {-3, 1 | null, null})` \
Eine Liste mit zwei list items, einer Arraylänge von 3, den Elementen 0 und null im ersten Array und keine Elemente im zweiten Array: `(2, {0, null | null}) <-> (0, { | null, null, null})`

DISCLAIMER: Das Durchlaufen der Tests ist keine Garantie dafür, dass die Aufgaben vollständig korrekt implementiert sind.

<br>

## ListOfArraysItemTest

### .classDefinitionCorrect()
Überprüft, ob die Definition der Klasse `ListOfArraysItem` korrekt ist. Testet, dass …
- die Klasse nicht `public` ist
- die Klasse generisch ist und einen Typparameter T hat
- die im Übungsblatt vorausgesetzten Attribute vorhanden und richtig implementiert sind

<br>

## ListOfArraysTest

Setzt voraus, dass `ListOfArraysItem` richtig definiert ist.

### .classDefinitionCorrect()
Überprüft, ob die Definition der Klasse `ListOfArrays` korrekt ist. Testet, dass …
- die Klasse `public` ist
- die Klasse generisch ist und einen Typparameter T hat
- die Klasse einen leeren Konstruktor hat
- die Klasse das Interface `List<T>` implementiert und nicht abstrakt ist
- die im Übungsblatt vorausgesetzten Attribute vorhanden und richtig implementiert sind
- die Klasse die Methode `readArrayLength()` besitzt und diese richtig definiert ist

<br>

### ReadArrayLengthTests

Testet die Methode `ListOfArrays.readArrayLength(String)`. Setzt voraus, dass `ListOfArrays` richtig definiert ist.

##### .testIOException(String)
Überprüft, ob die Methode eine `IOException` wirft, sollte es sich beim eingelesenen String nicht um eine ganze Zahl handeln.

##### .testNegativeArraySizeException(String)
Überprüft, ob die Methode eine `NegativeArraySizeException` wirft, sollte eine negative Zahl eingelesen werden.

##### .testRandomValidNumber(String, int)
Überprüft, ob die Methode erfolgreich eine ganze Zahl einliest (darunter immer 0), diese dem Attribut `LENGTH_OF_ALL_ARRAYS` zuweist und die Datei wieder schließt.

<br>

### ContainsTests

Testet die Methode `ListOfArrays.contains(Object)`. Setzt voraus, dass `ListOfArrays` richtig definiert ist.

##### .testNoList()
Überprüft, ob die Methode korrekt mit einer uninitialisierten Liste umgeht.

##### .testEmptyList()
Überprüft, ob die Methode korrekt mit einer leeren Liste umgehen kann.

##### .testSingleArrayList()
Überprüft, ob die Methode korrekt mit einer Liste umgehen kann, die nur ein list item hat.

##### .testHiddenElements()
Überprüft, ob die Methode korrekt mit einer Liste umgehen kann, die mehrere Elemente hat, aber als einelementig deklariert ist.

##### .testMultiItemList()
Überprüft, ob die Methode korrekt mit einer Liste umgehen kann, die über mehrere Arrays verteilt ist.

<br>

### AddTests

Testet die Methode `ListOfArrays.add(T)`. Setzt voraus, dass `ListOfArrays` richtig definiert ist.

##### .testNoList()
Überprüft, ob die Methode korrekt eine neue Liste erstellen kann und darin neue Elemente anlegen kann.

##### .testSingleArrayList()
Überprüft, ob die Methode korrekt Elemente einer nichtleeren Liste hinzufügen kann.

##### .testArrayFull()
Überprüft, ob die Methode korrekterweise ein neues `ListOfArraysItem`-Objekt erstellt, wenn das Array des letzten Glieds voll ist und die Elemente 50/50 auf die beiden Arrays verteilt.

##### .testMultiItemList()
Überprüft, ob die Methode korrekt mit einer mehrgliedrigen Liste umgehen kann.

##### .testOddArrayLength()
Überprüft, ob die Methode korrekt mit einer Liste umgehen kann, wenn das Attribut `LENGTH_OF_ALL_ARRAYS` ungerade ist.

<br>

### AddAllTests

Testet die Methode `ListOfArrays.addAll(Collection)`. Setzt voraus, dass `ListOfArrays` richtig definiert ist.

##### .testNullCollection()
Überprüft, ob die Methode eine NullPointerException wirft, wenn der aktuale Parameter null ist.

##### .testNoList()
Überprüft, ob die Methode korrekt eine neue Liste mit Elementen erstellen kann.

##### .testEmptyList()
Überprüft, ob die Methode korrekt einer leeren Liste eine leere Collection und eine nichtleere Collection hinzufügen kann.

##### .testSingleArrayList()
Überprüft, ob die Methode korrekt Elemente einer nichtleeren Liste hinzufügen kann.

##### .testArrayFull()
Überprüft, ob die Methode korrekterweise neue `ListOfArraysItem`-Objekte erstellt, wenn das Array des letzten Glieds voll ist oder dies anderweitig nötig ist.

<br>

### GetTests

Testet die Methode `ListOfArrays.get(int)`. Setzt voraus, dass `ListOfArrays` richtig definiert ist.

##### .testBadIndices()
Überprüft, ob die Methode für ungültige Indizes eine `IndexOutOfBoundsException` wirft.

##### .testEmptyList()
Überprüft, ob die Methode korrekt mit einer leeren Liste umgehen kann.

##### .testSingleArrayList()
Überprüft, ob die Methode korrekt mit einer nichtleeren Liste umgehen kann.

##### .testMultiItemList()
Überprüft, ob die Methode korrekt mit einer mehrgliedrigen Liste umgehen kann.

<br>

### .testIterator()

Setzt voraus, dass `ListOfArrays` und `ListOfArraysIterator` richtig definiert sind. Überprüft, ob die Methode `ListOfArrays.iterator()` richtig funktioniert. Testet, dass …
- die Methode nicht `null` zurückgibt
- die Methode eine Instanz von `ListOfArraysIterator` zurückgibt

<br>

## ListOfArraysIteratorTest

Setzt voraus, dass `ListOfArraysItem` und `ListOfArrays` richtig definiert sind.

### .classDefinitionCorrect()

Überprüft, ob die Definition der Klasse `ListOfArraysIterator` korrekt ist. Testet, dass …
- die Klasse `public` ist
- die Klasse generisch ist und einen Typparameter T hat
- die Klasse das Interface `Iterator<T>` implementiert und nicht abstrakt ist

<br>

### HasNextTests

Setzt voraus, dass `ListOfArrays` und `ListOfArrays.iterator()` richtig definiert sind.

##### .testNoList()
Überprüft, ob die Methode eine `NoSuchElementException` wirft, wenn die Liste nicht initialisiert ist.

##### .testEmptyList()
Überprüft, ob die Methode eine `NoSuchElementException` wirft, wenn die Liste leer ist.

##### .testOneElementIterator()
Überprüft, ob die Methode korrekt mit einer einelementigen Liste umgehen kann und ob sie eine `NoSuchElementException` wirft, wenn der Iterator am Ende der Liste angekommen ist.

##### .testMultiItemList()
Überprüft, ob die Methode korrekt mit mehrgliedrigen Listen umgehen kann und ob sie eine `NoSuchElementException` wirft, wenn der Iterator am Ende der Liste angekommen ist.

<br>

### HasNextTests

Setzt voraus, dass `ListOfArrays`, `ListOfArrays.iterator()` und `ListOfArraysIterator.next()` richtig definiert sind.

##### .testNoList()
Überprüft, ob die Methode korrekt mit einer nicht initialisierten Liste umgehen kann.

##### .testEmptyList()
Überprüft, ob die Methode korrekt mit einer leeren Liste umgehen kann.

##### .testOneElementIterator()
Überprüft, ob die Methode korrekt mit einer einelementigen Liste umgehen kann.

##### .testMultiItemList()
Überprüft, ob die Methode korrekt mit mehrgliedrigen Listen umgehen kann.

<br>

### UnsupportedOperationsTests

Setzt voraus, dass `ListOfArrays` und `ListOfArrays.iterator()` richtig definiert sind.

##### .testForEachRemaining()
Überprüft, ob eine `UnsupportedOperationException` beim Aufruf der Methode `ListOfArrayIterator.forEachRemaining(Consumer)` geworfen wird.

##### .testRemove()
Überprüft, ob eine `UnsupportedOperationException` beim Aufruf der Methode `ListOfArrayIterator.remove()` geworfen wird.
